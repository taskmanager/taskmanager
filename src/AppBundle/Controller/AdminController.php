<?php

namespace AppBundle\Controller;

use Criteria;
use AppBundle\Propel\WorkItem;
use AppBundle\Propel\WorkItemQuery;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/admin")
 * @Template()
 */
class AdminController extends Controller
{
    /**
     * @Route("/", name="homepage_admin")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $yesterday = date('Y-m-d', strtotime('yesterday'));

        $last_working_day = date('Y-m-d', strtotime('last weekday'.date('now')));

        $tasks = WorkItemQuery::create()
            ->filterByType(WorkItem::TASK)
            ->filterByUpdatedAt(array(
                'min' => $yesterday." 23:59:59",
            ))
            ->orderByUpdatedAt(Criteria::DESC)
            ->joinWithI18n($request->getLocale())
            ->find();

        $tasks_pending = WorkItemQuery::create()
            ->filterByType(WorkItem::TASK)
            ->filterByUpdatedAt(array(
                'min' => $last_working_day." 00:00:00",
                'max' => $yesterday." 23:59:59",
            ))
            ->orderByUpdatedAt(Criteria::DESC)
            ->find();

        $problems = WorkItemQuery::create()
            ->filterByType(WorkItem::PROBLEM)
            ->orderByUpdatedAt(Criteria::DESC)
            ->find();

        return array(
            'tasks' => $tasks,
            'tasks_pending' => $tasks_pending,
            'problems' => $problems,
        );
    }
}
